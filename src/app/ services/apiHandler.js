import {withRouter} from 'react-router-dom';
import axios from 'axios';

const API_URL = " https://stgapi.omnicuris.com/api/v3/";

export const performRequest = (method, url, params, data, login = true) => {
    const body = method === 'get' || method === 'delete' || method === 'put' || method == 'patch' ? 'params' : 'data';

    const config = {
        method,
        url,
        baseURL: API_URL,
        [body]: params || {},
    };
    if (method === 'put' || method == 'patch') {
        config.data = data
    }
    config.headers = {
        "Content-Type": "application/json; charset=utf-8",
        "hk-access-token": "89e684ac-7ade-4cd8-bbdf-419a92f4cc5f"
    };

    // if (login) {
    //     config.headers.Authorization = "Bearer " + localStorage.getItem('accessToken')
    // }
    return axios.request(config)
    // Add a request interceptor

};

axios.interceptors.response.use((response) => {
    return response
});
// (performRequest);
