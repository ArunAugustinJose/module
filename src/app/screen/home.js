import React from 'react'
import {
    Button,
    Col,
    Spinner
} from "react-bootstrap";
import "../style/style.css"
import {performRequest} from "../ services/apiHandler";
import ReactPlayer from 'react-player'
import {IoMdTime} from "react-icons/io";
import {FaFacebookF, FaTwitter, FaLinkedinIn, FaWhatsapp, FaYoutube} from "react-icons/fa";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import {Modal, ModalBody, Row} from "reactstrap";

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: {max: 4000, min: 3000},
        items: 8
    },
    desktop: {
        breakpoint: {max: 3000, min: 1024},
        items: 6
    },
    tablet: {
        breakpoint: {max: 1024, min: 464},
        items: 4
    },
    mobile: {
        breakpoint: {max: 464, min: 0},
        items: 1
    }
};

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            course_name: '',
            course_slug: '',
            width: 0,
            height: 0,
            loader: false,
            video: '',
            icon: "",
            modules: [],
            expertDetails: [],
            description: '',
            modal: false,
            modal_image: '',
            modal_module_duration: '',
            chapters: [],
            accredited_image: '',
            accredited_name: ''

        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        this.setState({loader: true});

        this.getModules()
    }

    getModules = () => {
        performRequest('get', 'courses?courseSlug=thyroid-in-pregnancy')
            .then(response => {
                console.log("HOME", response.data.courseDetails)
                this.setState({
                    course_name: response.data.courseDetails.name,
                    course_slug: "Introduction",
                    video: response.data.courseDetails.introVideo,
                    modules: response.data.courseDetails.modules.sort((a, b) => (a['displayOrder'] < b['displayOrder'] ? -1 : 1)),
                    description: response.data.courseDetails.description,
                    accredited_image: response.data.courseDetails.courseOrganizations[0].displayImage,
                    accredited_name: response.data.courseDetails.courseOrganizations[0].name
                });
                this.getExperts()
            })
            .catch(error => {
                this.setState({loader: false})
            });
    };

    getExperts = () => {
        performRequest('get', 'courses/thyroid-in-pregnancy/experts')
            .then(response => {
                console.log("HOME2", response);
                this.setState({
                    expertDetails: response.data.expertDetails,
                    loader: false
                })
            }).catch(error => {
            this.setState({loader: false})
        });
    };

    getModuleDetails = (id) => {
        performRequest('get', 'courses?courseSlug=thyroid-in-pregnancy&moduleId=' + id)
            .then(response => {
                console.log("expert", response.data)
                this.setState({
                    chapters: response.data.lessonDetails[0].userChapterDetails.sort((a, b) => (a['displayOrder'] < b['displayOrder'] ? -1 : 1)),
                })

            })
            .catch(error => {
                this.setState({loader: false})
            });
    };

    modal_toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    };

    handleClick = (module) => {
        console.log("##", module)
        this.getModuleDetails(module.id);
        this.setState({
            course_name: module.title,
            course_slug: module.name,
            modal_image: module.moduleExperts[0].profilePic,
            modal_module_duration: module.durationStr,
            modal: true
        })
    };

    handleChapterClick = (item) =>{
        this.setState({video: item.content,
        modal: false})
    };

    render() {
        return (
            this.state.loader ?
                <div className='container-fluid'>
                    <Spinner animation="grow" size='lg' className='spinner'/>
                </div>
                :
                <div className='container-fluid'>
                    <div className='row col-lg-8 cl-md-8 col-sm-12 course-name mt-1 ml-2 module-title'>
                        {this.state.course_name + ": "}<span className='blue-text'>&nbsp;{this.state.course_slug}</span>
                    </div>
                    <Row className='pl-0 ml-2 mb-2'>
                        <Col lg={7} md={6} sm={12} xl={7} className='pl-0 mt-4'>
                            <ReactPlayer url={this.state.video} controls className='video-container' width={'100%'}
                                         height={'100%'}/>
                        </Col>
                        <Col lg={1} md={1} sm={0}></Col>
                        <Col lg={4} md={5} sm={12} xl={4} className='pl-0 mt-4'>
                            <div className='course-name module-main pl-lg-2 pr-lg-2 pl-xl-4 pr-xl-4 pl-md-2 pr-md-2'>
                                <div className='row card-6 ml-0 mr-0 mb-4 mt-2 module-container'
                                     onClick={() => this.getModules()}>
                                    <div className='col-3 col-xl-2 col-lg-3 align-self-center'>
                                        <img className=" rounded-circle image-container mg-thumbnail"
                                             width={45} height={45}
                                             src={require('../assets/shapes-and-symbols.png')}/>
                                    </div>
                                    <div className='col-9 col-xl-10 col-lg-9 module-title m-text pl-2 pr-0'>
                                        <span className="mb-0 m-text font-weight-bold">Introduction</span>
                                    </div>
                                </div>
                                {
                                    this.state.modules.map(item => {
                                        return (
                                            <div className='row card-6 ml-0 mr-0 p-2 mb-4 module-container'
                                                 onClick={() => this.handleClick(item)}>
                                                <div className='col-3 col-xl-2 col-lg-3 align-self-center'>
                                                    <img
                                                        className="rounded-circle image-container object-fit img-thumbnail"
                                                        width={45} height={45}
                                                        src={item.moduleExperts[0].profilePic}/>
                                                </div>
                                                <div className='col-9 col-xl-10 col-lg-9 module-title m-text pl-2 pr-0'>
                                                    {item.title + " - "}<span
                                                    className="mb-0 blue-text">{item.name}</span>
                                                </div>

                                                <div className='duration'>
                                                    <span className='duration-container'>
                                                        <IoMdTime size={16} color='#5285ec'
                                                                  className='mb-1'/>{" " + item.durationStr}
                                                    </span>
                                                </div>
                                            </div>
                                        )

                                    })
                                }
                            </div>
                        </Col>
                    </Row>

                    <div className='row col-sm-12 col-md-10 col-lg-8  mt-5 ml-2'>
                        <h4 className='underline pb-1'>Description</h4>
                        <div className='position-absolute social-container'>
                            <div className='expert-container social'>
                                <FaFacebookF size={17} color='#6784bd' classname='social-icon'/>
                            </div>
                            <div className='expert-container social'>
                                <FaTwitter size={17} color='#6784bd' classname='social-icon'/>
                            </div>
                            <div className='expert-container social'>
                                <FaLinkedinIn size={17} color='#6784bd' classname='social-icon'/>
                            </div>
                            <div className='expert-container social'>
                                <FaYoutube size={17} color='#6784bd' classname='social-icon'/>
                            </div>
                            <div className='expert-container social'>
                                <FaWhatsapp size={17} color='#6784bd' classname='social-icon'/>
                            </div>
                        </div>
                    </div>
                    <div className='row col-lg-8 col0xl-7 col-md-9 col-sm-12 mt-2 ml-2'>
                        <span>{this.state.description}</span>
                    </div>

                    <div className='row col-12 mt-4 ml-2'>
                        <h4 className='underline pb-1'>Experts Panel</h4>
                    </div>
                    <Row className='container-fluid mt-2 mb-5'>
                        <Col className='ml-4 mr-4 text-center'>
                            <Carousel responsive={responsive}>
                                {this.state.expertDetails.map(item => {
                                    return (
                                        <div className='ml-4 mr-4 mt-4 expert-container'>
                                            <img className="img-thumbnail image-container mb-1"
                                                 width={120} height={120} src={item.profilePic}/>
                                            <p className='mb-1 blue-text expert-name'>{item.expertName}</p>
                                            <p className='m-text expert-qul'>{item.qualification}</p>
                                        </div>
                                    )
                                })
                                }
                            </Carousel>
                        </Col>
                    </Row>

                   {/*Popup Modal*/}
                    <Modal isOpen={this.state.modal} toggle={this.modal_toggle} className={this.props.className}>
                        <ModalBody>
                            <div className='container'>
                                <div className='row col-12 title mr-0 ml-0'>
                                    <img src={require('../assets/signs.png')} className='title-icon' width={25}
                                         height={25}/><h5>Credit Points Applicable</h5>
                                </div>
                                <div className='course-name chapter-main row mt-4 mb-4 chapter-container'>
                                    <div className='card-6 row m-2 p-2'>
                                        <div className='row ml-0 mr-0 p-2 mb-4 '
                                             onClick={() => this.handleClick()}>
                                            <div className='col-3 col-xl-2 col-lg-3 align-self-center'>
                                                <img className="rounded-circle image-container object-fit img-thumbnail"
                                                     width={45} height={45}
                                                     src={this.state.modal_image}/>
                                            </div>
                                            <div className='col-9 col-xl-10 col-lg-9 module-title m-text pl-2 pr-0'>
                                                {this.state.course_name + " - "}<span
                                                className="mb-0 blue-text">{this.state.course_slug}</span>
                                            </div>

                                            <div className='duration'>
                                                    <span className='duration-container'>
                                                        <IoMdTime size={16} color='#5285ec'
                                                                  className='mb-1'/>{" " + this.state.modal_module_duration}
                                                    </span>
                                            </div>
                                        </div>
                                        {
                                            this.state.chapters.map(item => {
                                                return (
                                                    <div className='row col-12 card-6 ml-0 mr-0 p-2 mb-4 chapter-container'
                                                         onClick={() => this.handleChapterClick(item)}>
                                                        <div className='col-3 align-self-center'>
                                                            <img
                                                                className="rounded-circle image-container object-fit img-thumbnail"
                                                                width={45} height={45}
                                                                src={item.chapterExperts[0].profilePic}/>
                                                        </div>
                                                        <div className='col-9 module-title m-text pl-2 pr-0'>
                                                            <div className='row col-12'>{item.title}</div>
                                                            <div className='row col-12 mb-0 blue-text'>{item.slug}</div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                                <div className='row col-12 mt-4'>
                                    <h5 className='underline pb-1'>Accredited By</h5>
                                </div>
                                <div className='mt-4 text-center'>
                                    <img className='card-6 p-3' src={this.state.accredited_image}/>
                                </div>
                                <div className='row col-12 mt-4'>
                                    <h5 className='text-center'>{this.state.accredited_name}</h5>
                                </div>
                            </div>

                        </ModalBody>
                    </Modal>
                </div>
        )
    }
}

export default Home;

/**
 * Created@ 13/05/2020
 * #Arun Jose
 */